################################################################

<h1>HD44780 LCD DISPLAY</h1>

################################################################

This is my lybrary based on Miroslaw Kardas blue Book.
 
It was made for learning how to use and program LCD HD44780 display.
 
Feel fre to contribute if you see it useful.

################################################################

<h2>Main keywords</h2>

################################################################

void lcd_init

void lcd_cls

void lcd_str

void lcd_str_P

void lcd_str_E

void lcd_home

void lcd_int

void lcd_hex

void lcd_locate

void lcd_cursor_off

void lcd_cursor_on

void lcd_cursor_blink_on

void lcd_cursor_blink_off

################################################################

Base on GNU GPLv.3 licence.

###############################################################
